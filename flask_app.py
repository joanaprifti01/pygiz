
# A very simple Flask Hello World app for you to get started with...

from flask import Flask , render_template , request
from pruducts import Product


app = Flask(__name__)
my_products=[Product('shirts',20,50),Product('jeans',30,40),Product('shoes',10,5)]


@app.route('/')
def index():

    return render_template('index.html')

@app.route('/table')
def table():
    title='Products table'
    #search_term = request.args['search']
    search_term = request.args.get('search' , '')
    if search_term :
        search_products=[]
        for product in my_products:
            if search_term.lower() in product.name.lower() :
                search_products.append(product)
            return render_template('table.html',TITLE=title,MY_PRODUCTS=search_products)

    else:
        return render_template('table.html',TITLE=title,MY_PRODUCTS=my_products)
